import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatDto } from './dto/create-cat.dto';
import { UpdateCatDto } from './dto/update-cat.dto';
import { Cat } from './entities/cat.entity';

@Controller('cats')
export class CatsController {
  constructor(private readonly catsService: CatsService) {}

  @Post()
  async create(@Body() createCatDto: CreateCatDto) : Promise<Cat> {
    const createdCat = await this.catsService.create(createCatDto);
    return createdCat
  }

  @Get()
  async findAll() : Promise<Cat[]> {
    return await this.catsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number) : Promise<Cat> {
    return await this.catsService.findOne(id);
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() updateCatDto: UpdateCatDto) : Promise<Cat> {
    return await this.catsService.update(id, updateCatDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: number) : Promise<Cat> {
    return await this.catsService.remove(id);
  }
}
