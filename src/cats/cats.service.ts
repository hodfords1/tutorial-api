import { Injectable, NotFoundException } from '@nestjs/common';
import { Cat } from './entities/cat.entity';
import { UpdateCatDto } from './dto/update-cat.dto';
import { CreateCatDto } from './dto/create-cat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CatsService {
  constructor(
    @InjectRepository(Cat)
    private catsRepository: Repository<Cat>,
  ) {}

  async create(cat: CreateCatDto) : Promise<Cat> {
    const createdCat = this.catsRepository.create(cat);
    return await this.catsRepository.save(createdCat);
  }

  async findAll() : Promise<Cat[]> {
    return await this.catsRepository.find();
  }
 
  async findOne(id: number) : Promise<Cat> {
    const findCat = await this.findCat(id);
    return findCat;
  }

  async update(id: number, updateCat: UpdateCatDto) : Promise<Cat>  {
    const findCat = await this.findCat(id);
    const updatedCat = { ...findCat, ...updateCat};
    return await this.catsRepository.save(updatedCat);
  }

  async remove(id: number) : Promise<Cat>  {
    const removeCat = await this.findCat(id);
    return await this.catsRepository.remove(removeCat);
  }

  private async findCat(id: number) : Promise<Cat> {
    const findCat = await this.catsRepository.findOne({where: {id}});
    if(!findCat) {
      if(!findCat){
        throw new NotFoundException('Could not find cat.');
      }
    }
    return findCat;
  }
}
